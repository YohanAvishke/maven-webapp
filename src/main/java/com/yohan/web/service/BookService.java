/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.yohan.web.service;

import com.yohan.web.dao.model.BookDTO;
import com.yohan.web.service.model.Book;
import com.yohan.web.service.exception.ParseException;
import com.yohan.web.service.model.Catalog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yohan.web.dao.DaoService;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXB;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

public class BookService {
    final static Logger logger = Logger.getLogger(BookService.class);

    private BookDTO convertToDto(Book book) {
        ModelMapper modelMapper = new ModelMapper();
        BookDTO bookDTO = modelMapper.map(book, BookDTO.class);
        return bookDTO;
    }

    public Catalog serialize(String catalogXml) {
        Catalog catalog = JAXB.unmarshal(new StringReader(catalogXml), Catalog.class);
        ObjectMapper mapper = new ObjectMapper();
        DaoService service = new DaoService();

        for (Book book : catalog.getBooks()) {
            BookDTO bookDTO = convertToDto(book);
            service.addBook(bookDTO);
        }

        try {
            mapper.writeValue(System.out, catalog);
        } catch (IOException e) {
            logger.fatal(e.getMessage());
        }

        return catalog;
    }

    public boolean validate(String catalog) {
        ParseException parseException = new ParseException();
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(new InputSource(new StringReader(catalog)));
            String path = document.getElementsByTagName("context").item(0).getTextContent();

            Schema schema = factory.newSchema(new File(path));
            Validator validator = schema.newValidator();
            validator.setErrorHandler(parseException);
            validator.validate(new StreamSource(new StringReader(catalog)));
        } catch (ParserConfigurationException e) {

        } catch (IOException e) {

        } catch (SAXException e) {
            parseException.setValid(false);
            logger.fatal(e.getMessage());
        }

        return parseException.isValid();
    }
}
