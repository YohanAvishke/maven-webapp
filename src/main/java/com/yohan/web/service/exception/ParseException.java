/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.yohan.web.service.exception;

import org.apache.log4j.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ParseException extends BaseException implements ErrorHandler {
    private static final long SerialVersionUID = 10l;

    final static Logger logger = Logger.getLogger(ParseException.class);

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        logger.warn(exception.getMessage());
        this.setValid(false);
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        logger.error(exception.getMessage());
        this.setValid(false);
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
    }
}
