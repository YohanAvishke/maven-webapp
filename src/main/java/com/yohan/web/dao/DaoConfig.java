/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.yohan.web.dao;

import com.yohan.web.dao.model.BookDTO;
import com.zaxxer.hikari.HikariDataSource;
import io.ebean.EbeanServer;
import io.ebean.EbeanServerFactory;
import io.ebean.config.ServerConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

public class DaoConfig {
    private static final String DB_USERNAME = "db.username";
    private static final String DB_PASSWORD = "db.password";
    private static final String DB_URL = "db.url";
    private static final String DB_DRIVER_CLASS = "driver.class.name";

    private static Properties properties = null;
    private static HikariDataSource dataSource;
    private static ServerConfig sc;
    private static EbeanServer server;

    public static EbeanServer getServerConfig() {

        sc = new ServerConfig();
        properties = new Properties();
        try {
            properties.load(new FileInputStream("/Users/yohanavishke/Documents/Projects/" +
                                                "GitLab/YohanAvishke/maven-webapp/src/main/" +
                                                "resources/db.properties"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        dataSource = new HikariDataSource();

        dataSource.setDriverClassName(properties.getProperty(DB_DRIVER_CLASS));

        dataSource.setJdbcUrl(properties.getProperty(DB_URL));
        dataSource.setUsername(properties.getProperty(DB_USERNAME));
        dataSource.setPassword(properties.getProperty(DB_PASSWORD));

        dataSource.setMinimumIdle(2);
        dataSource.setMaximumPoolSize(10);
        dataSource.setAutoCommit(false);
        try {
            dataSource.setLoginTimeout(10);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        sc.loadFromProperties();
        sc.addClass(BookDTO.class);
        sc.setDataSource(dataSource); // set hikari data source to ebean

        server = EbeanServerFactory.create(sc);


        return server;
    }
}
