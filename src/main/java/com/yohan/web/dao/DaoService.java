/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.yohan.web.dao;

import com.yohan.web.dao.model.BookDTO;
import io.ebean.EbeanServer;
import org.apache.log4j.Logger;

public class DaoService {
    final static Logger logger = Logger.getLogger(DaoService.class);
    EbeanServer server = DaoConfig.getServerConfig();

    public void addBook(BookDTO bookDTO) {

//        try (Statement st = DaoConfig.getConnections().createStatement()) {
//
//            String SQL = "SELECT *FROM bookDTO";
//            ResultSet rs = st.executeQuery(SQL);
//            while (rs.next()) {
//                logger.fatal(rs.getInt("id"));
//            }
//        } catch (SQLException e) {
//            logger.fatal("CP "+e.getMessage());
//            try {
//                DaoConfig.connectionRollBack();// roll back connection
//            } catch (SQLException ex) {
//                logger.fatal("CP roll back "+e.getMessage());
//            }
//        }
        server.save(bookDTO);
    }
}
