/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.yohan.web.api;

import com.yohan.web.service.BookService;
import com.yohan.web.service.model.Catalog;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/book")
public class BookController implements BookControllerInterface {

    private BookService manager = new BookService();

    @Override
    public Response validate(String catalog) {
        boolean valid = manager.validate(catalog);
        if (!valid) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        return Response.ok().build();
    }

    @Override
    public Response serialize(String catalog) {
        Catalog catalogJson = null;
        boolean valid = manager.validate(catalog);
        if (!valid) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        } else {
            catalogJson = manager.serialize(catalog);
        }
        return Response.ok().entity(catalogJson).build();
    }
}
